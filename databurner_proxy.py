# Program Imports
import socket
import socks
import urllib2
import time
import datetime
import threading
import multiprocessing
import os
import pickle

################################################################################

# File Logging
def log_info (logfile, msg):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  print "\n** %s ** : %s" % (timestamp, msg)
  if logfile != None:
    with open(logfile, "a") as log:
      log.write("\n** %s ** : %s" % (timestamp, msg))
  return

################################################################################

# File Saving & Loading

#file save
def file_save (filename, mission):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  try:
    f = open(filename, "wb+")
    pickle.dump(mission, f)
    f.close()
    print "\n** %s ** : %s" % (timestamp, "Saved configuration to %s." % filename)
  except:
    print "\n** %s ** : %s" % (timestamp, "Failure to save configuration to %s." % filename)
  return

#file load
def file_load (filename, mission):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  try:
    f = open(filename, "rb")
    data = pickle.load(f)
    f.close()
    print "\n** %s ** : %s" % (timestamp, "Loaded configuration from %s." % filename)
    #iterate through dictionary to copy items into mission parameter
    mission.clear
    for key, value in data.iteritems():
      mission[key] = value
  except:
    print "\n** %s ** : %s" % (timestamp, "Failure to load configuration from %s." % filename)
  return  

################################################################################

# Effect Execution

# prepare tasking
def build_task (mission, task):
  log_info(None, "INFO : building new thread task from mission")
  task.clear()
  task["valid"] = True
  if "threads" in mission:
    try:
      task["threads"] = int(mission["threads"])
    except:
      task["valid"] = False
  else:
    task["threads"] = 1
  if "url" in mission:
    task["url"] = mission["url"]
  else:
    task["valid"] = False
  if "delay" in mission:
    try:
      task["delay"] = int(mission["delay"])
    except:
      task["valid"] = False
  else:
    task["delay"] = 0
  if "limit" in mission:
    try:
      task["limit"] = (int(int(mission["limit"]) * 1000000) / task["threads"])
    except:
      task["valid"] = False
  else:
    task["valid"] = False
  if "proxy" in mission:
    try:
      proxy = mission["proxy"].split(":")
      task["proxy_host"] = proxy[0]
      task["proxy_port"] = int(proxy[1])
      if (task["proxy_port"] < 1024) or (task["proxy_port"] > 65535):
        task["valid"] = False
    except:
      task["valid"] = False
  else:
    task["proxy"] = None
  if "log" in mission:
    task["log"] = mission["log"]
  else:
    task["log"] = "log.txt"
  if task["valid"]:
    log_info(task["log"], "INFO : configured task for URL:\t %s" % task["url"])
    log_info(task["log"], "INFO : configured task for delay:\t %s" % task["delay"])
    log_info(task["log"], "INFO : configured task for limit:\t %s" % task["limit"])
    log_info(task["log"], "INFO : configured task for threads:\t %s" % task["threads"])
    if "proxy_host" in task:
      log_info(task["log"], "INFO : configured task for proxy:\t %s:%s" % (task["proxy_host"], task["proxy_port"]))
  return

# execution thread
def execution_thread (lock, queue, task):
  local_pid = os.getpid()
  lock.acquire()
  log_info(task["log"], "INFO : executing mission on PID[%s]" % local_pid)
  lock.release()
  bytes = 0
  noerror = True
  while (bytes < task["limit"]) and (noerror):
    try:
      if "proxy_host" in task:
        SOCKS5_PROXY_HOST = task["proxy_host"]
        SOCKS5_PROXY_PORT = task["proxy_port"]
        socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
        socket.socket = socks.socksocket
    except:
      lock.acquire()
      log_info(task["log"], "PID[%s] : proxy configuration failure from %s:%s" % (local_pid, task["proxy_host"], task["proxy_port"]))
      lock.release()
      noerror = False
    try:
      url = urllib2.urlopen(task["url"])
      data = url.read()
      url.close
    except:
      lock.acquire()
      log_info(task["log"], "PID[%s] : URL read failure from %s" % (local_pid, task["url"]))
      lock.release()
      noerror = False
    if noerror:
      size = len(data)
      queue.put([local_pid, size])
      bytes = bytes + size
      lock.acquire()
      log_info(task["log"], "PID[%s] : %s bytes read from %s" % (local_pid, size, url.geturl()))
      lock.release()
      time.sleep(task["delay"])
  return bytes

# execute test
def execute_test (mission):
  task = {}
  bytes = 0
  build_task(mission, task)
  #defaulting particular parameters for single instance test
  task["limit"] = 1
  task["delay"] = 0
  if task["valid"]:
    default_socket = socket.socket
    lock = multiprocessing.Lock()
    queue = multiprocessing.Queue()
    p = multiprocessing.Process(target=execution_thread, args=(lock, queue, task))
    p.start()
    p.join()
    if queue.empty == False:
      data = queue.get()
      bytes = data[1]
    else:
      bytes = 0
    socket.socket = default_socket
  else:
    log_info(task["log"], "ERROR : invalid parameters detected")
  return

# execute mission
def execute_live (mission):
  task = {}
  bytes = 0
  build_task(mission, task)
  if task["valid"]:
    default_socket = socket.socket
    procs = []
    lock = multiprocessing.Lock()
    queue = multiprocessing.Queue()
    for counter in range(task["threads"]):
      p = multiprocessing.Process(target=execution_thread, args=(lock, queue, task))
      p.start()
      procs.append(p)
    bytes = 0
    while True:
      while queue.empty() == False:
        data = queue.get()
        bytes = bytes + data[1]
      counter = 0
      for index in range(len(procs)):
        p = procs[index]
        if p.is_alive():
          counter = counter + 1
      if counter == 0:
        break
      else:
        lock.acquire()
        log_info(task["log"], "INFO : %s live processes" % (counter,))
        log_info(task["log"], "INFO : %s bytes downloaded" % (bytes,))
        lock.release()
      time.sleep(5)
    socket.socket = default_socket
    log_info(task["log"], "INFO : %s total bytes read from %s" % (bytes, task["url"]))
  else:
    log_info(task["log"], "ERROR : invalid parameters detected")
  return

################################################################################

# Configuration Changes

# Set URL
def param_set_url (mission):
  print "\n"
  mission["url"] = raw_input("Enter URL: ")
  return

# Set Delay
def param_set_delay (mission):
  print "\n"
  mission["delay"] = raw_input("Enter Delay (in seconds): ")
  return

def param_set_limit (mission):
  print "\n"
  mission["limit"] = raw_input("Enter Limit (in MB): ")
  return

# Set Threads
def param_set_threads (mission):
  print "\n"
  mission["threads"] = raw_input("Enter Threads: ")
  return

# Set Proxy
def param_set_proxy (mission):
  print "\n"
  mission["proxy"] = raw_input("Enter Proxy: ")
  return

# Set Logging
def param_set_logging (mission):
  print "\n"
  mission["log"] = raw_input("Enter Log File: ")
  return

################################################################################

# Reset Configuration

def param_reset (mission):
  mission.clear()
  return

################################################################################

# Action Menus

def menu_change_parameters (mission):
  while True:
    print "\n**************************************************"
    print "Change Parameters Menu"
    if "url" in mission:
      print "(a) : Set URL\t\t - %s" % mission["url"]
    else:
      print "(a) : Set URL\t\t - REQUIRED"
    if "delay" in mission:
      print "(b) : Set Delay\t\t - %ss" % mission["delay"]
    else:
      print "(b) : Set Delay\t\t - DEFAULT to 0s"
    if "limit" in mission:
      print "(c) : Set Limit\t\t - %sMB" % mission["limit"]
    else:
      print "(c) : Set Limit\t\t - REQUIRED"
    if "threads" in mission:
      print "(d) : Set Threads\t - %s" % mission["threads"]
    else:
      print "(d) : Set Threads\t - DEFAULT to 1"
    if "proxy" in mission:
      print "(e) : Set Proxy\t\t - %s" % mission["proxy"]
    else:
      print "(e) : Set Proxy\t\t - OPTIONAL - IP:port"
    if "log" in mission:
      print "(f) : Set Logging\t - %s" % mission["log"]
    else:
      print "(f) : Set Logging\t - DEFAULT to log.txt"
    print "(x) : Exit Parameters Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'a':
      param_set_url(mission)
    elif selection == 'b':
      param_set_delay(mission)
    elif selection == 'c':
      param_set_limit(mission)
    elif selection == 'd':
      param_set_threads(mission)
    elif selection == 'e':
      param_set_proxy(mission)
    elif selection == 'f':
      param_set_logging(mission)
    elif selection == 'x':
      break
  return

def menu_file (mission):
  while True:
    print "\n**************************************************"
    print "Save/Load from File Menu"
    print "(l) : Load from File"
    print "(s) : Save to File"
    print "(x) : Exit Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'l':
      print "\n"
      filename = raw_input("Enter Filename: ")
      print mission
      file_load(filename, mission)
      print mission
    elif selection == 's':
      print "\n"
      filename = raw_input("Enter Filename: ")
      file_save(filename, mission)
    elif selection == 'x':
      break
  return     

def menu_reset_parameters (mission):
  while True:
    print "\n**************************************************"
    print "Reset Parameters Menu"
    print "(y) : Confirm Reset"
    print "(x) : Exit Reset Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'y':
      param_reset(mission)
      break
    elif selection == 'x':
      break
  return

def menu_execute (mission):
  while True:
    print "\n**************************************************"
    print "Execute Mission Menu"
    print "(t) : Test Single Instance"
    print "(y) : Confirm Live Execute"
    print "(x) : Exit Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 't':
      execute_test(mission)
    elif selection == 'y':
      execute_live(mission)
    elif selection == 'x':
      break
  return     

def menu_main (mission):
  while True:
    print "\n**************************************************"
    print "Main Menu"
    print "(c) : Configure Parameters"
    print "(f) : Save/Load from File"
    print "(r) : Reset to Default"
    print "(x) : Execute Mission"
    print "(q) : Quit"
    selection = raw_input("\nPlease Select: ")
    if selection == 'c':
      menu_change_parameters(mission)
    elif selection == 'f':
      menu_file(mission)
    elif selection == 'r':
      menu_reset_parameters(mission)
    elif selection == 'x':
      menu_execute(mission)
    elif selection == 'q':
      break
  return

################################################################################

# handle non-interactive execution mode

def auto_main (mission):
  filename = sys.argv[1]
  file_load(filename, mission)
  execute_live(mission)
  return

################################################################################

print "Data Consumption - TCP Puller"
mission = {}
if len(sys.argv) > 1:
  auto_main(mission)
else:
  menu_main(mission)
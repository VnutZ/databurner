# Program Imports
import socket
import time
import datetime
import threading
import multiprocessing
import os
import sys
import random
import pickle

################################################################################

# File Logging
def log_info (logfile, msg):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  print "\n** %s ** : %s" % (timestamp, msg)
  if logfile != None:
    with open(logfile, "a") as log:
      log.write("\n** %s ** : %s" % (timestamp, msg))
  return

################################################################################

# File Saving & Loading

#file save
def file_save (filename, mission):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  try:
    f = open(filename, "wb+")
    pickle.dump(mission, f)
    f.close()
    print "\n** %s ** : %s" % (timestamp, "Saved configuration to %s." % filename)
  except:
    print "\n** %s ** : %s" % (timestamp, "Failure to save configuration to %s." % filename)
  return

#file load
def file_load (filename, mission):
  timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
  try:
    f = open(filename, "rb")
    data = pickle.load(f)
    f.close()
    print "\n** %s ** : %s" % (timestamp, "Loaded configuration from %s." % filename)
    #iterate through dictionary to copy items into mission parameter
    mission.clear
    for key, value in data.iteritems():
      mission[key] = value
  except:
    print "\n** %s ** : %s" % (timestamp, "Failure to load configuration from %s." % filename)
  return  

################################################################################

# Effect Execution

# prepare tasking
def build_task (mission, task):
  log_info(None, "INFO : building new thread task from mission")
  task.clear()
  task["valid"] = True
  if "threads" in mission:
    try:
      task["threads"] = int(mission["threads"])
    except:
      task["valid"] = False
  else:
    task["threads"] = 1
  if "ip" in mission:
    task["ip"] = mission["ip"]
  else:
    task["valid"] = False
  if "port" in mission:
    try:
      task["port"] = int(mission["port"])
      if (task["port"] < 1) or (task["port"] > 65535):
        task["valid"] = False
    except:
      task["valid"] = False
  else:
    task["port"] = 0
  if "size" in mission:
    try:
      task["size"] = int(mission["size"])
      if (task["size"] < 1) or (task["size"] > 65507):
        task["valid"] = False
    except:
      task["valid"] = False
  else:
    task["size"] = 0
  if "delay" in mission:
    try:
      task["delay"] = int(mission["delay"])
    except:
      task["valid"] = False
  else:
    task["delay"] = 0
  if "limit" in mission:
    try:
      task["limit"] = (int(int(mission["limit"]) * 1000000) / task["threads"])
    except:
      task["valid"] = False
  else:
    task["valid"] = False
  if "log" in mission:
    task["log"] = mission["log"]
  else:
    task["log"] = "log.txt"
  if task["valid"]:
    log_info(task["log"], "INFO : configured task for IP:\t %s" % task["ip"])
    if task["port"] == 0:
      log_info(task["log"], "INFO : configured task for Port:\t RANDOM")
    else:
      log_info(task["log"], "INFO : configured task for Port:\t %s" % task["port"])
    if task["size"] == 0:
      log_info(task["log"], "INFO : configured task for Size:\t RANDOM")
    else:
      log_info(task["log"], "INFO : configured task for Size:\t %s" % task["size"])
    log_info(task["log"], "INFO : configured task for delay:\t %s" % task["delay"])
    log_info(task["log"], "INFO : configured task for limit:\t %s" % task["limit"])
    log_info(task["log"], "INFO : configured task for threads:\t %s" % task["threads"])
  return

# execution thread
def execution_thread (lock, queue, task):
  local_pid = os.getpid()
  lock.acquire()
  log_info(task["log"], "INFO : executing mission on PID[%s]" % local_pid)
  lock.release()
  bytes = 0
  noerror = True
  while (bytes < task["limit"]) and (noerror):
    try:
      if task["port"] == 0:
        target_port = random.randint(1,65535)
      else:
        target_port = task["port"]
      target = (task["ip"], target_port)
      if task["size"] == 0:
        target_size = random.randint(1,4096)
      else:
        target_size = task["size"]
      packet = os.urandom(target_size)      
      sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      sock.sendto(packet, target)
      sock.close
    except:
      lock.acquire()
      log_info(task["log"], "PID[%s] : UDP send failure to %s:%s" % (local_pid, task["ip"], target_port))
      lock.release()
      noerror = False
    if noerror:
      size = len(packet)
      queue.put([local_pid, size])
      bytes = bytes + size
      lock.acquire()
      log_info(task["log"], "PID[%s] : %s bytes send to %s:%s" % (local_pid, size, task["ip"], target_port))
      lock.release()
      time.sleep(task["delay"])
  return bytes

# execute test
def execute_test (mission):
  task = {}
  bytes = 0
  build_task(mission, task)
  #defaulting particular parameters for single instance test
  task["limit"] = 1
  task["delay"] = 0
  if task["valid"]:
    default_socket = socket.socket
    lock = multiprocessing.Lock()
    queue = multiprocessing.Queue()
    p = multiprocessing.Process(target=execution_thread, args=(lock, queue, task))
    p.start()
    p.join()
    if queue.empty == False:
      data = queue.get()
      bytes = data[1]
    else:
      bytes = 0
    socket.socket = default_socket
  else:
    log_info(task["log"], "ERROR : invalid parameters detected")
  return

# execute mission
def execute_live (mission):
  task = {}
  bytes = 0
  build_task(mission, task)
  if task["valid"]:
    default_socket = socket.socket
    procs = []
    lock = multiprocessing.Lock()
    queue = multiprocessing.Queue()
    for counter in range(task["threads"]):
      p = multiprocessing.Process(target=execution_thread, args=(lock, queue, task))
      p.start()
      procs.append(p)
    bytes = 0
    while True:
      while queue.empty() == False:
        data = queue.get()
        bytes = bytes + data[1]
      counter = 0
      for index in range(len(procs)):
        p = procs[index]
        if p.is_alive():
          counter = counter + 1
      if counter == 0:
        break
      else:
        lock.acquire()
        log_info(task["log"], "INFO : %s live processes" % (counter,))
        log_info(task["log"], "INFO : %s bytes downloaded" % (bytes,))
        lock.release()
      time.sleep(5)
    socket.socket = default_socket
    log_info(task["log"], "INFO : %s total bytes sent to %s" % (bytes, task["ip"]))
  else:
    log_info(task["log"], "ERROR : invalid parameters detected")
  return

################################################################################

# Configuration Changes

# Set URL
def param_set_ip (mission):
  print "\n"
  mission["ip"] = raw_input("Enter IP: ")
  return

# Set Port
def param_set_port (mission):
  print "\n"
  mission["port"] = raw_input("Enter Port (1-65535): ")
  return

# Set Size
def param_set_size (mission):
  print "\n"
  mission["size"] = raw_input("Enter Packet Size (in bytes): ")
  return

# Set Delay
def param_set_delay (mission):
  print "\n"
  mission["delay"] = raw_input("Enter Delay (in seconds): ")
  return

def param_set_limit (mission):
  print "\n"
  mission["limit"] = raw_input("Enter Limit (in MB): ")
  return

# Set Threads
def param_set_threads (mission):
  print "\n"
  mission["threads"] = raw_input("Enter Threads: ")
  return

# Set Logging
def param_set_logging (mission):
  print "\n"
  mission["log"] = raw_input("Enter Log File: ")
  return

################################################################################

# Reset Configuration

def param_reset (mission):
  mission.clear()
  return

################################################################################

# Action Menus

def menu_change_parameters (mission):
  while True:
    print "\n**************************************************"
    print "Change Parameters Menu"
    if "ip" in mission:
      print "(a) : Set IP\t\t - %s" % mission["ip"]
    else:
      print "(a) : Set IP\t\t - REQUIRED"
    if "port" in mission:
      print "(b) : Set Port\t\t - %s" % mission["port"]
    else:
      print "(b) : Set Port\t\t - DEFAULT to random"
    if "size" in mission:
      print "(c) : Set Size\t\t - %s bytes" % mission["size"]
    else:
      print "(c) : Set Size\t\t - DEFAULT to random up to 4096 bytes"
    if "delay" in mission:
      print "(d) : Set Delay\t\t - %ss" % mission["delay"]
    else:
      print "(d) : Set Delay\t\t - DEFAULT to 0s"
    if "limit" in mission:
      print "(e) : Set Limit\t\t - %sMB" % mission["limit"]
    else:
      print "(e) : Set Limit\t\t - REQUIRED"
    if "threads" in mission:
      print "(f) : Set Threads\t - %s" % mission["threads"]
    else:
      print "(f) : Set Threads\t - DEFAULT to 1"
    if "log" in mission:
      print "(h) : Set Logging\t - %s" % mission["log"]
    else:
      print "(h) : Set Logging\t - DEFAULT to log.txt"
    print "(x) : Exit Parameters Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'a':
      param_set_ip(mission)
    elif selection == 'b':
      param_set_port(mission)
    elif selection == 'c':
      param_set_size(mission)
    elif selection == 'd':
      param_set_delay(mission)
    elif selection == 'e':
      param_set_limit(mission)
    elif selection == 'f':
      param_set_threads(mission)
    elif selection == 'h':
      param_set_logging(mission)
    elif selection == 'x':
      print mission
      break
  return

def menu_file (mission):
  while True:
    print "\n**************************************************"
    print "Save/Load from File Menu"
    print "(l) : Load from File"
    print "(s) : Save to File"
    print "(x) : Exit Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'l':
      print "\n"
      filename = raw_input("Enter Filename: ")
      print mission
      file_load(filename, mission)
      print mission
    elif selection == 's':
      print "\n"
      filename = raw_input("Enter Filename: ")
      file_save(filename, mission)
    elif selection == 'x':
      break
  return     

def menu_reset_parameters (mission):
  while True:
    print "\n**************************************************"
    print "Reset Parameters Menu"
    print "(y) : Confirm Reset"
    print "(x) : Exit Reset Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 'y':
      param_reset(mission)
      break
    elif selection == 'x':
      break
  return

def menu_execute (mission):
  while True:
    print "\n**************************************************"
    print "Execute Mission Menu"
    print "(t) : Test Single Instance"
    print "(y) : Confirm Live Execute"
    print "(x) : Exit Menu"
    selection = raw_input("\nPlease Select: ")
    if selection == 't':
      execute_test(mission)
    elif selection == 'y':
      execute_live(mission)
    elif selection == 'x':
      break
  return     

def menu_main (mission):
  while True:
    print "\n**************************************************"
    print "Main Menu"
    print "(c) : Configure Parameters"
    print "(f) : Save/Load from File"
    print "(r) : Reset to Default"
    print "(x) : Execute Mission"
    print "(q) : Quit"
    selection = raw_input("\nPlease Select: ")
    if selection == 'c':
      menu_change_parameters(mission)
    elif selection == 'f':
      menu_file(mission)
    elif selection == 'r':
      menu_reset_parameters(mission)
    elif selection == 'x':
      menu_execute(mission)
    elif selection == 'q':
      break
  return

################################################################################

# handle non-interactive execution mode

def auto_main (mission):
  filename = sys.argv[1]
  file_load(filename, mission)
  execute_live(mission)
  return

################################################################################

print "Data Consumption - UDP Pumper"
mission = {}
if len(sys.argv) > 1:
  auto_main(mission)
else:
  menu_main(mission)